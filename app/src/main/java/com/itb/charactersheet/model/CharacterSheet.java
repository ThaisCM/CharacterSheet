package com.itb.charactersheet.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "character_sheet")
public class CharacterSheet {

        @PrimaryKey(autoGenerate = true)
        int id;

        @ColumnInfo(name = "player")
        String player;

        @ColumnInfo(name = "chronicle")
        String chronicle;

        @ColumnInfo(name = "name")
        String name;

        @ColumnInfo(name = "age")
        int age;

        @ColumnInfo(name = "race")
        String race;

        @ColumnInfo(name = "level")
        int level;

        @ColumnInfo(name = "profession")
        String profession;

        @ColumnInfo(name = "height")
        double height;

        @ColumnInfo(name = "weight")
        double weight;

        @ColumnInfo(name = "gender")
        String gender;

        @ColumnInfo(name = "strength")
        float strength;

        @ColumnInfo(name = "constitution")
        float constitution;

        @ColumnInfo(name = "dexterity")
        float dexterity;

        @ColumnInfo(name = "intelligence")
        float intelligence;

        @Ignore
        public CharacterSheet(int id) {
                this.id = id;
        }

        public CharacterSheet(String player, String chronicle, String name, int age, String race, int level, String profession, double height, double weight, String gender, float strength, float constitution, float dexterity, float intelligence) {
                this.player = player;
                this.chronicle = chronicle;
                this.name = name;
                this.age = age;
                this.race = race;
                this.level = level;
                this.profession = profession;
                this.height = height;
                this.weight = weight;
                this.gender = gender;
                this.strength = strength;
                this.constitution = constitution;
                this.dexterity = dexterity;
                this.intelligence = intelligence;
        }

        public int getId() {
                return id;
        }

        public void setId(int id) {
                this.id = id;
        }

        public String getPlayer() {
                return player;
        }

        public void setPlayer(String player) {
                this.player = player;
        }

        public String getChronicle() {
                return chronicle;
        }

        public void setChronicle(String chronicle) {
                this.chronicle = chronicle;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public int getAge() {
                return age;
        }

        public void setAge(int age) {
                this.age = age;
        }

        public String getRace() {
                return race;
        }

        public void setRace(String race) {
                this.race = race;
        }

        public int getLevel() {
                return level;
        }

        public void setLevel(int level) {
                this.level = level;
        }

        public String getProfession() {
                return profession;
        }

        public void setProfession(String profession) {
                this.profession = profession;
        }

        public double getHeight() {
                return height;
        }

        public void setHeight(double height) {
                this.height = height;
        }

        public double getWeight() {
                return weight;
        }

        public void setWeight(double weight) {
                this.weight = weight;
        }

        public String getGender() {
                return gender;
        }

        public void setGender(String gender) {
                this.gender = gender;
        }

        public float getStrength() {
                return strength;
        }

        public void setStrength(float strength) {
                this.strength = strength;
        }

        public float getConstitution() {
                return constitution;
        }

        public void setConstitution(float constitution) {
                this.constitution = constitution;
        }

        public float getDexterity() {
                return dexterity;
        }

        public void setDexterity(float dexterity) {
                this.dexterity = dexterity;
        }

        public float getIntelligence() {
                return intelligence;
        }

        public void setIntelligence(float intelligence) {
                this.intelligence = intelligence;
        }
}

//https://www.modiphius.net/products/vampire-the-masquerade-5th-edition-character-sheets-free#mz-expanded-view-207585369532
