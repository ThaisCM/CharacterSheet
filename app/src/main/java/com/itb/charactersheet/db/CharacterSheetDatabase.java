package com.itb.charactersheet.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.itb.charactersheet.dao.CharacterSheetDao;
import com.itb.charactersheet.model.CharacterSheet;

@Database(entities = {CharacterSheet.class}, version = 2, exportSchema  = false)
public abstract class CharacterSheetDatabase extends RoomDatabase {
    public abstract CharacterSheetDao characterSheetDao();

    private static volatile CharacterSheetDatabase INSTANCE;

    private static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE 'character_sheet' ADD COLUMN 'strength' FLOAT NOT NULL DEFAULT 0");
            database.execSQL("ALTER TABLE 'character_sheet' ADD COLUMN 'constitution' FLOAT NOT NULL DEFAULT 0");
            database.execSQL("ALTER TABLE 'character_sheet' ADD COLUMN 'dexterity' FLOAT NOT NULL DEFAULT 0");
            database.execSQL("ALTER TABLE 'character_sheet' ADD COLUMN 'intelligence' FLOAT NOT NULL DEFAULT 0");
        }
    };

    public static CharacterSheetDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CharacterSheetDatabase.class){
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), CharacterSheetDatabase.class, "charactersheetdb").allowMainThreadQueries().addMigrations(MIGRATION_1_2).build();
                }
            }
        }
        return INSTANCE;
    }
}
