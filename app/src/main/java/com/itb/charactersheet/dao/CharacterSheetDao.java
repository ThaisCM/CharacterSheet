package com.itb.charactersheet.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.itb.charactersheet.model.CharacterSheet;

import java.util.List;


@Dao
public interface CharacterSheetDao {
        @Insert
        void insert(CharacterSheet  characterSheet);

        @Delete
        void delete(CharacterSheet characterSheet);

        @Update
        void update(CharacterSheet characterSheet);

        @Query("SELECT * FROM character_sheet")
        LiveData<List<CharacterSheet>> getAllCharacterSheets();

        @Query("SELECT * FROM character_sheet WHERE id = :id")
        CharacterSheet getCharacterSheetById(int id);

        @Query("SELECT * FROM character_sheet WHERE id = :id")
        LiveData<CharacterSheet> getCharacterSheetsById(int id);

        //Filters
        @Query("SELECT * FROM character_sheet WHERE player = :player")
        LiveData<List<CharacterSheet>> getCharacterSheetsByPlayer(String player);

        @Query("SELECT * FROM character_sheet WHERE name = :name")
        LiveData<List<CharacterSheet>> getCharacterSheetsByName(String name);

        @Query("SELECT * FROM character_sheet WHERE level = :level")
        LiveData<List<CharacterSheet>> getCharacterSheetsByExactLevel(int level);

        @Query("SELECT * FROM character_sheet WHERE level < :level")
        LiveData<List<CharacterSheet>> getCharacterSheetsByLowerLevel(int level);

        @Query("SELECT * FROM character_sheet WHERE level > :level")
        LiveData<List<CharacterSheet>> getCharacterSheetsByGreaterLevel(int level);

        //Order
        @Query("SELECT * FROM character_sheet ORDER BY player")
        LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByPlayer();

        @Query("SELECT * FROM character_sheet ORDER BY name")
        LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByName();

        @Query("SELECT * FROM character_sheet ORDER BY level")
        LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByLevel();

}
