package com.itb.charactersheet.screens.creation;

import android.app.Application;

import com.itb.charactersheet.model.CharacterSheet;
import com.itb.charactersheet.repository.CharacterSheetRepository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class CharacterSheetCreationViewModel extends AndroidViewModel {
    private CharacterSheetRepository characterSheetRepository;
    private boolean isUpdate = false;

    public CharacterSheetCreationViewModel(@NonNull Application application) {
        super(application);
        this.characterSheetRepository = new CharacterSheetRepository(application);
    }

    public void insert(CharacterSheet characterSheet){
        if(isUpdate) characterSheetRepository.update(characterSheet);
        else characterSheetRepository.insert(characterSheet);
    }

    public boolean isUpdate(int characterSheetId){
        isUpdate = characterSheetId != -1;
        return isUpdate;
    }

    public LiveData<CharacterSheet> getCharacterSheetsById(int characterSheetId){
        return characterSheetRepository.getCharacterSheetsById(characterSheetId);
    }

    public LiveData<List<CharacterSheet>> getAllCharacterSheets(){
        return characterSheetRepository.getAllCharacterSheets();
    }
}
