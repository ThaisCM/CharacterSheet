package com.itb.charactersheet.screens.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.itb.charactersheet.R;
import com.itb.charactersheet.model.CharacterSheet;

import java.util.List;

import butterknife.OnClick;

public class CharacterSheetAdapter extends RecyclerView.Adapter<CharacterSheetAdapter.CharacterSheetViewHolder> {
    List<CharacterSheet> characterSheetList;
    OnCharacterSheetClickListener onCharacterSheetClickListener;

    public CharacterSheetAdapter(List<CharacterSheet> characterSheetList) {
        this.characterSheetList = characterSheetList;
    }

    public CharacterSheetAdapter() {};

    public void setOnCharacterSheetClickListener(OnCharacterSheetClickListener onCharacterSheetClickListener){
        this.onCharacterSheetClickListener = onCharacterSheetClickListener;
    }

    public void setCharacterSheetList(List<CharacterSheet> characterSheetList){
        this.characterSheetList = characterSheetList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(characterSheetList != null) return characterSheetList.size();
        else return 0;
    }

    @NonNull
    @Override
    public CharacterSheetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.character_sheet_row,parent,false);
        return new CharacterSheetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterSheetViewHolder holder, int position) {
        CharacterSheet characterSheet = characterSheetList.get(position);
        holder.tvRowPlayer.setText(characterSheet.getPlayer());
        holder.tvRowName.setText(characterSheet.getName());
        holder.tvRowChronicle.setText(characterSheet.getChronicle());
        holder.tvRowLevel.setText(String.valueOf(characterSheet.getLevel()));
        String title = "Character Sheet:";
        holder.tvTitle.setText(title);
    }

    public class CharacterSheetViewHolder extends RecyclerView.ViewHolder {
        TextView tvRowPlayer;
        TextView tvRowName;
        TextView tvRowChronicle;
        TextView tvRowLevel;
        TextView tvTitle;
        public CharacterSheetViewHolder(@NonNull View view) {
            super(view);
            view.setOnClickListener(this::onItemClicked);
            tvRowPlayer = view.findViewById(R.id.tvRowPlayer);
            tvRowName = view.findViewById(R.id.tvRowName);
            tvRowChronicle = view.findViewById(R.id.tvRowChronicle);
            tvRowLevel = view.findViewById(R.id.tvRowLevel);
            tvTitle = view.findViewById(R.id.tvRowCharacterSheet);
        }

        @OnClick(R.id.character_sheet_row)
        public void onItemClicked(View view){
            CharacterSheet characterSheet = characterSheetList.get(getAdapterPosition());
            if(onCharacterSheetClickListener != null) onCharacterSheetClickListener.onCharacterClicked(characterSheet, view);
        }
    }

    public interface OnCharacterSheetClickListener {
        void onCharacterClicked(CharacterSheet characterSheet, View view);
    }

}
