package com.itb.charactersheet.screens.details;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.itb.charactersheet.R;
import com.itb.charactersheet.model.CharacterSheet;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CharacterSheetDetailsFragment extends Fragment {

    private CharacterSheetDetailsViewModel mViewModel;
    @BindView(R.id.tvPlayer)
    TextView tvPlayer;
    @BindView(R.id.tvChronicle)
    TextView tvChronicle;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvRace)
    TextView tvRace;
    @BindView(R.id.tvGender)
    TextView tvGender;
    @BindView(R.id.tvAge)
    TextView tvAge;
    @BindView(R.id.tvProfession)
    TextView tvProfession;
    @BindView(R.id.tvLevel)
    TextView tvLevel;
    @BindView(R.id.tvHeight)
    TextView tvHeight;
    @BindView(R.id.tvWeight)
    TextView tvWeight;
    @BindView(R.id.rbShowStrength)
    RatingBar rbStrength;
    @BindView(R.id.rbShowConstitution)
    RatingBar rbConstitution;
    @BindView(R.id.rbShowDexterity)
    RatingBar rbDexterity;
    @BindView(R.id.rbShowIntelligence)
    RatingBar rbIntelligence;
//    @BindView(R.id.tvTextCharacterSheet)
//    TextView tvTitle;

    private int characterSheetId;

    public static CharacterSheetDetailsFragment newInstance() {
        return new CharacterSheetDetailsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.character_sheet_details_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CharacterSheetDetailsViewModel.class);

        characterSheetId = CharacterSheetDetailsFragmentArgs.fromBundle(getArguments()).getId();
        mViewModel.loadCharacterSheet(characterSheetId);
        CharacterSheet characterSheet = mViewModel.getCharacterSheet();

        display(characterSheet);
    }

    private void display(CharacterSheet characterSheet) {
        tvPlayer.setText(characterSheet.getPlayer());
        tvChronicle.setText(characterSheet.getChronicle());
        tvName.setText(characterSheet.getName());
        tvRace.setText(characterSheet.getRace());
        tvGender.setText(characterSheet.getGender());
        tvAge.setText(String.valueOf(characterSheet.getAge()));
        tvProfession.setText(characterSheet.getProfession());
        tvLevel.setText(String.valueOf(characterSheet.getLevel()));
        tvHeight.setText(new StringBuilder().append(characterSheet.getHeight()).append(" m").toString());
        tvWeight.setText(new StringBuilder().append(characterSheet.getWeight()).append(" kg").toString());
        rbStrength.setRating(characterSheet.getStrength());
        rbConstitution.setRating(characterSheet.getConstitution());
        rbDexterity.setRating(characterSheet.getDexterity());
        rbIntelligence.setRating(characterSheet.getIntelligence());
    }

    @OnClick(R.id.fabUpdate)
    public void onUpdateFABClicked(View view){
        NavDirections action = CharacterSheetDetailsFragmentDirections.actionUpdate(characterSheetId);
        Navigation.findNavController(view).navigate(action);
    }

    @OnClick(R.id.fabDelete)
    public void onDeleteFABClicked(View view){
        mViewModel.delete(characterSheetId);
        Navigation.findNavController(view).navigate(R.id.characterSheetListFragment);
    }

}
