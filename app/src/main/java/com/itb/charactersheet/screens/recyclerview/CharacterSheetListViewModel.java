package com.itb.charactersheet.screens.recyclerview;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.itb.charactersheet.model.CharacterSheet;
import com.itb.charactersheet.repository.CharacterSheetRepository;

import java.util.List;


public class CharacterSheetListViewModel extends AndroidViewModel {
    private CharacterSheetRepository repository;

    public CharacterSheetListViewModel(@NonNull Application application) {
        super(application);
        this.repository = new CharacterSheetRepository(application);
    }

    public LiveData<List<CharacterSheet>> getAllCharacterSheets() {
        return repository.getAllCharacterSheets();
    }

    //Filters
    public LiveData<List<CharacterSheet>> getCharacterSheetsByPlayer(String player){
        return repository.getCharacterSheetsByPlayer(player);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByName(String name){
        return repository.getCharacterSheetsByName(name);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByExactLevel(int level){
        return repository.getCharacterSheetsByExactLevel(level);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByLowerLevel(int level){
        return repository.getCharacterSheetsByLowerLevel(level);
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsByGreaterLevel(int level){
        return repository.getCharacterSheetsByGreaterLevel(level);
    }

    //Order
    public LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByPlayer(){
        return repository.getCharacterSheetsOrderedByPlayer();
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByName(){
        return repository.getCharacterSheetsOrderedByName();
    }


    public LiveData<List<CharacterSheet>> getCharacterSheetsOrderedByLevel(){
        return repository.getCharacterSheetsOrderedByLevel();
    }
}
