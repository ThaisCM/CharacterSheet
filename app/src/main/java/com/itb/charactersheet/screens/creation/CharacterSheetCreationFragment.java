package com.itb.charactersheet.screens.creation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.itb.charactersheet.R;
import com.itb.charactersheet.model.CharacterSheet;

public class CharacterSheetCreationFragment extends Fragment {

    private CharacterSheetCreationViewModel mViewModel;

    @BindView(R.id.inputPlayer)
    EditText inputPlayer;
    @BindView(R.id.inputChronicle)
    EditText inputChronicle;
    @BindView(R.id.inputName)
    EditText inputName;
    @BindView(R.id.inputRace)
    EditText inputRace;
    @BindView(R.id.inputGender)
    EditText inputGender;
    @BindView(R.id.inputAge)
    EditText inputAge;
    @BindView(R.id.inputProfession)
    EditText inputProfession;
    @BindView(R.id.inputLevel)
    EditText inputLevel;
    @BindView(R.id.inputHeight)
    EditText inputHeight;
    @BindView(R.id.inputWeight)
    EditText inputWeight;
    @BindView(R.id.rbShowStrength)
    RatingBar rbStrength;
    @BindView(R.id.rbShowConstitution)
    RatingBar rbConstitution;
    @BindView(R.id.rbShowDexterity)
    RatingBar rbDexterity;
    @BindView(R.id.rbShowIntelligence)
    RatingBar rbIntelligence;

    private int characterSheetId;

    public static CharacterSheetCreationFragment newInstance() {
        return new CharacterSheetCreationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.character_sheet_creation_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CharacterSheetCreationViewModel.class);

        if(mViewModel.isUpdate(characterSheetId)){
            LiveData<CharacterSheet> characterSheetLiveData = mViewModel.getCharacterSheetsById(characterSheetId);
            characterSheetLiveData.observe(this, this::fillEditTextWithData);
        }
    }

    private void fillEditTextWithData(CharacterSheet characterSheet) {
        inputPlayer.setText(characterSheet.getPlayer());
        inputChronicle.setText(characterSheet.getChronicle());
        inputName.setText(characterSheet.getName());
        inputRace.setText(characterSheet.getRace());
        inputGender.setText(characterSheet.getGender());
        inputAge.setText(String.valueOf(characterSheet.getAge()));
        inputProfession.setText(characterSheet.getProfession());
        inputLevel.setText(String.valueOf(characterSheet.getLevel()));
        inputHeight.setText(String.valueOf(characterSheet.getHeight()));
        inputWeight.setText(String.valueOf(characterSheet.getWeight()));
        rbStrength.setRating(characterSheet.getStrength());
        rbConstitution.setRating(characterSheet.getConstitution());
        rbDexterity.setRating(characterSheet.getDexterity());
        rbIntelligence.setRating(characterSheet.getIntelligence());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        characterSheetId = CharacterSheetCreationFragmentArgs.fromBundle(getArguments()).getId();
    }

    @OnClick(R.id.btnSave)
    public void onButtonSaveClicked(View view){
        CharacterSheet characterSheet = getCharacterSheetAtributes();
        if(characterSheetId != -1) characterSheet.setId(characterSheetId);
        mViewModel.insert(characterSheet);
        Navigation.findNavController(view).navigate(R.id.action_view_list);
    }

    private CharacterSheet getCharacterSheetAtributes() {
        String player = inputPlayer.getText().toString().trim();
        String chronicle = inputChronicle.getText().toString().trim();
        String name = inputName.getText().toString().trim();
        String race = inputRace.getText().toString().trim();
        String gender = inputGender.getText().toString().trim();

        String ageStr = inputAge.getText().toString().trim();
        int age = 0;
        if(!ageStr.isEmpty()) age = Integer.valueOf(ageStr);

        String profession = inputProfession.getText().toString().trim();

        String levelStr = inputLevel.getText().toString().trim();
        int level = 0;
        if(!levelStr.isEmpty()) level = Integer.valueOf(levelStr);

        String heightStr = inputHeight.getText().toString().trim();
        double height = 0;
        if(!heightStr.isEmpty()) height = Double.valueOf(heightStr);

        String weightStr = inputWeight.getText().toString().trim();
        double weight = 0;
        if(!weightStr.isEmpty()) weight = Double.valueOf(weightStr);

        float strength = rbStrength.getRating();
        float constitution = rbConstitution.getRating();
        float dexterity = rbDexterity.getRating();
        float intelligence = rbIntelligence.getRating();
        if((strength+constitution+dexterity+intelligence)>level) {
            Toast.makeText(getActivity(),("You have " + level + " points to fill the attributes. Check your character. Values established to 0"),Toast.LENGTH_LONG).show();
            return new CharacterSheet(player, chronicle, name, age, race, level, profession, height, weight, gender, 0, 0, 0, 0);
        } else {
            return new CharacterSheet(player, chronicle, name, age, race, level, profession, height, weight, gender, strength, constitution, dexterity, intelligence);
        }
    }

}
