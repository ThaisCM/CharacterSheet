package com.itb.charactersheet;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itb.charactersheet.preferences.CharacterSheetSharedPreferences;

public class MainActivity extends AppCompatActivity {
    private static long back_pressed;
    private Switch switchMode;
    CharacterSheetSharedPreferences sharedPreferences;
    private FloatingActionButton btnFeedback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = new CharacterSheetSharedPreferences(this);
        if(sharedPreferences.loadNightModeState()){
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        switchMode = (Switch) findViewById(R.id.switchMode);
        if(sharedPreferences.loadNightModeState()){
            switchMode.setChecked(true);
        }
        switchMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    sharedPreferences.setNightModeState(true);
                    recreate();
                } else {
                    sharedPreferences.setNightModeState(false);
                    recreate();
                }
            }
        });

        btnFeedback = findViewById(R.id.fabFeedback);
        btnFeedback.setOnClickListener(this::onButtonFeedbackClicked);
    }




    @Override
    public void onBackPressed() {
        if (back_pressed + 2000 < System.currentTimeMillis()) {
            super.onBackPressed();
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(R.string.app_name);
            builder.setIcon(R.mipmap.ic_launcher);
            builder.setMessage("Do you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        back_pressed = System.currentTimeMillis();
    }


    private void onButtonFeedbackClicked(View view) {
        final Intent _Intent = new Intent(android.content.Intent.ACTION_SEND);
        _Intent.setType("text/html");
        _Intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{ getString(R.string.mail_feedback_email) });
        _Intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.mail_feedback_subject));
        _Intent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.mail_feedback_message));
        startActivity(Intent.createChooser(_Intent, getString(R.string.title_send_feedback)));
    }
}
